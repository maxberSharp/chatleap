﻿/**********************************************
 (c) 2015 ClickWorx LLC. All rights reserved.
 **********************************************/
var path         = require('path');
var webpack      = require('webpack');
var autoprefixer = require('autoprefixer');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin')


module.exports = {
    entry: ['./app/app','./scss/style'],
    output: {
        path: './build',
        publicPath: '/build/',
        filename: '[name].bundle.js'
    },
   
    devtool: 'eval',
    module: {
        preLoaders: [{
            test: /\.jsx?$/,
            loaders: ['babel-loader','eslint'],
            include: path.resolve(__dirname, "app")
        }],
        loaders: [
            // Transform JSX in .jsx files
            {
                test: /\.jsx?$/,
                include: [path.resolve(__dirname, "app")],
                loader: 'babel-loader',
                query: {
                    plugins: ['transform-decorators-legacy',"transform-object-assign","transform-function-bind"],
                    presets: ['es2015', 'react', 'stage-0']
                }
            }, {
                test: /\.js$/,
                loader: 'imports?define=>false'
            }, //disable amd modules, since we are using commonJS
            {
                test: /\.css$/,                
                loader: ExtractTextPlugin.extract('style-loader', 'css-loader?minimize')
            },
             {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract(
                    'style-loader', // backup loader when not building .css file
                    'css-loader!sass-loader' // loaders to preprocess CSS
                )
               
              },{
                  test: /\.(jpg|png)$/,
                loader: 'file-loader',
                include: './img'
                
              },
               {
                test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
                loader: "url?limit=10000&mimetype=application/font-woff"
            }, {
                test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
                loader: "url?limit=10000&mimetype=application/font-woff2"
            }, {
                test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                loader: "url?limit=10000&mimetype=application/octet-stream"
            }, {
                test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                loader: "file"
            }, {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                loader: "url?limit=10000&mimetype=image/svg+xml"
            }
        ],
    },
    node: {
        global: true
    },
    resolve: {
        extensions: ['', '.react.js', '.js', '.jsx', '.scss','.css'],
        modulesDirectories: [
            "app", "node_modules","scss"
        ]
        
    },
    externals: {
        jquery: 'jQuery'
    },
    plugins: [
        new ExtractTextPlugin("clinet.bundle.css"),
        new HtmlWebpackPlugin({
            title: 'chat',
            filename: 'index.html',
            template: 'index.html',
        }),
        new CopyWebpackPlugin([
            { from: 'img', to:'img'},
          ])
    
    ],
    postcss: [ autoprefixer() ]
};