import {makeActionCreator} from '../helpers/redux-helpers';
import chatService from '../services/ChatService';
import CircularJSON from 'circular-json';

export const INTERLOCATOR_INFO = 'interLocatorInfo';
export const ADD_MESSAGE = 'addMessage';
export const SET_INTERATOR_TAPING = 'isInteratorTaping';
export const ADD_ANALIZATOR_POINT = 'addAnalizatorPoint';

export const setInteratorInfo = makeActionCreator(INTERLOCATOR_INFO,'info');
export const addMessage = makeActionCreator(ADD_MESSAGE,'message');
export const setInteratorTaping = makeActionCreator(SET_INTERATOR_TAPING,'is_taping');
export const addAnalizatorPoint = makeActionCreator(ADD_ANALIZATOR_POINT,"point");

export function getInterlocutorInfo(){
    return  (dispatch) => {

        chatService.getInterlocutor().then(interlocutor=>{
            dispatch(setInteratorInfo(interlocutor));
        });

        
    }
}

export function startAnalizatorProcess(){
    return  (dispatch , getState) => {
      var progress = 0;
      var interval = setInterval(()=>{
          var point = {
            title:" point - " + progress,
            rate: Math.random() * 4 + 1,
            progress: progress
        }
          dispatch(addAnalizatorPoint(point));
           progress += 20;
         if(progress >= 101)
          clearInterval(interval);
      },1500);
    }
}

export function addTestMessage(message){
    return  (dispatch,getState) => {
        dispatch(addMessage(message)); 
        var interlocutor = getState().interlocutor.info;
        var msgType = getState().requestedMessageType + 1;

        if(interlocutor.id != message.user.id){  
            dispatch(setInteratorTaping(true));
            setTimeout(()=>{           
                dispatch(setInteratorTaping(false));
                dispatch(addMessage({
                    user:interlocutor,
                    type:msgType, 
                    text: getMessage(msgType, message.user.username, message.text), 
                    buttons:{"Yes":"Yes","No":"No"},
                    checkboxset:["Roommaters","Fire alarm","Burglar Alarm"]

                }));
                window.localStorage.setItem("state",CircularJSON.stringify(getState()));
            },2500);
        }

        
    }
}

const getMessage = (msgType, username, text) => {
    switch (msgType) {
        case 1:
            return "Choose one of next variants:" ;
        case 2:
            return "What is your Birthday?";
        case 3:
            return "Are you hangry?";
        case 4:
            return "What's your address?";                        
        default:
            return "Answer to " + username + " on message `" + text+"'";
    }
}
