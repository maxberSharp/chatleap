import {makeActionCreator} from '../helpers/redux-helpers';

export const ADD_USER = 'addUser';

export const addUser = makeActionCreator(ADD_USER,'user');