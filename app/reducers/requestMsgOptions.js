import {createReducer} from '../helpers/redux-helpers';
import * as Actions from '../actions/chatAction';

export default createReducer({},{
    [Actions.ADD_MESSAGE](state,action){
        switch(action.message.type){
            case 1:
             return action.message.checkboxset ;
            case 3: 
             return action.message.buttons;
            default:
             return state;
        }
        return {};
    }
  });