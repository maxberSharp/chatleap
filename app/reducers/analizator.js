import {createReducer} from '../helpers/redux-helpers';
import { combineReducers } from 'redux';
import * as Actions from '../actions/chatAction';

const PointsReducer = createReducer([],{
    [Actions.ADD_ANALIZATOR_POINT](state,action){        
        state.push(action.point);
        return [...state];
    }
});

const ProgressReducer = createReducer(0,{
    [Actions.ADD_ANALIZATOR_POINT](state,action){        
       return action.point.progress;        
    }
});

export default combineReducers({
    points: PointsReducer,
    progress: ProgressReducer
});