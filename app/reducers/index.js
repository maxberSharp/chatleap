import { combineReducers } from 'redux';
import MessagesReducer  from './messages';
import InterlocutorReducer from './interlocutor';
import RequestedMessageReducer from './requestedMessage';
import UserReducer from './user';
import RequestedOptionsReducer from './requestMsgOptions';
import AnalizatorReducer from './analizator';

export default combineReducers({
    messages: MessagesReducer,
    interlocutor: InterlocutorReducer,
    requestedMessageType: RequestedMessageReducer,
    requestedMessageOptions: RequestedOptionsReducer,
    user: UserReducer,
    analizator: AnalizatorReducer
})