import {createReducer} from '../helpers/redux-helpers';
import * as Actions from '../actions/chatAction';

export default createReducer(0,{
  [Actions.ADD_MESSAGE](state,action){
      return action.message.type;
  }
});