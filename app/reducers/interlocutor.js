import {createReducer} from '../helpers/redux-helpers';
import { combineReducers } from 'redux';
import * as Actions from '../actions/chatAction';

 const InterlocutorTypingReducer = createReducer(false,{
    [Actions.SET_INTERATOR_TAPING](store,action){     
        return  action.is_taping;
    }
});


 const InterlocutorInfoReducer = createReducer({},{
    [Actions.INTERLOCATOR_INFO](store,action){     
     return  action.info;
    }
});

export default combineReducers({
    info: InterlocutorInfoReducer,
    istyping:InterlocutorTypingReducer
})