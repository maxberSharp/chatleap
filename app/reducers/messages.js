import {createReducer} from '../helpers/redux-helpers';
import * as Actions from '../actions/chatAction';
export default createReducer([],{
    [Actions.ADD_MESSAGE](state,action){
        console.log(action);
        state.push(action.message);
        return [...state];
    }
});