export default function callAPIMiddleware({ dispatch, getState }) {
    return next => action => {
        const {
            types,
            callAPI,
            onSuccess,
            onFail,
            shouldCallAPI = () => true,
            payload = {}
        } = action
        
        if (!types) {
            // Normal action: pass it on
            return next(action)
        }
        
        if (!Array.isArray(types) ||
            types.length !== 3 ||
            !types.every(type => typeof type === 'string')) 
        {
            throw new Error('Expected an array of three string types.')
        }
            
        if (typeof callAPI !== 'function') {
            throw new Error('Expected fetch to be a function.')
        }
        
        if (!shouldCallAPI(getState())) {
            return
        }
        
        const [ requestType, successType, failureType ] = types
        if(requestType.length > 0){
            dispatch(Object.assign(payload, {
                type: requestType
            }))
        }
        
        return callAPI().then(
            response => {
                if(successType.length > 0){
                    dispatch(Object.assign(payload, {
                        response,
                        type: successType
                    }))
                }
                
                if(onSuccess && typeof onSuccess === 'function'){
                    onSuccess();
                }
            },
            error => {
                if(failureType.length > 0){
                    dispatch(Object.assign(payload, {
                        error,
                        type: failureType
                    }))
                }
                
                if(onFail && typeof onFail === 'function'){
                    onSuccess();
                }
            }
        )
    }
}