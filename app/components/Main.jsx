import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import { bindActionCreators } from 'redux';
import {connect} from 'react-redux';
import * as Actions from '../actions/chatAction';
import * as userActions from '../actions/userActions';
import Header from './Header';
import User from './User';
import Chat from './Chat';
import CheckBoxer from './controls/CheckBoxer';
import TextArea from './controls/TextArea';
import Autocomplete from './controls/Autocomplete';
import Datepicker from './controls/Datepicker';
import Buttons from './controls/Buttons';


@connect((store)=>({type:store.requestedMessageType,user:store.user, options: store.requestedMessageOptions }),
(dispatch)=>bindActionCreators(Object.assign(Actions,userActions),dispatch))

export default class Main extends Component {
    

    
    scrollToBottom (){
        const node = ReactDOM.findDOMNode(this.messagesEnd);
        node.scrollIntoView({ behavior: "smooth" });
      }
      
      componentDidMount() {
        this.scrollToBottom();
      }
      
      componentDidUpdate() {
        this.scrollToBottom();
      }

      componentWillMount(){
        this.props.addUser({
            id:1,
            username:"Test user",
            "photo":"../../img/img-02.png"
        });
       }
    render() {
        
        return (
            <div >
                <Header/>
                <div className="wrapper">
                        <div className="container">
                            <User/>
                            <Chat/>
                    
                        </div>
                </div>
                {this.props.type == 1 && <CheckBoxer options={this.props.options} onSendMessage={(msg)=>this.props.addTestMessage({user:this.props.user,text:msg,type:1, checkboxset:[]})}/>}
                {this.props.type == 2 && <Datepicker onSendMessage={(msg)=>this.props.addTestMessage({user:this.props.user,text:msg,type:2})} />}
                {this.props.type == 0 && <TextArea onSendMessage={(msg)=>this.props.addTestMessage({user:this.props.user,text:msg,type:0})}/>}
                {this.props.type == 4 && <Autocomplete onSendMessage={(msg)=>this.props.addTestMessage({user:this.props.user,text:msg,type:5})}/>}
                {this.props.type == 3 && <Buttons options={this.props.options} onSendMessage={(msg)=>this.props.addTestMessage({user:this.props.user,text:msg,type:3,buttons:{}})}/>}
                <div ref={i => this.messagesEnd = i}></div>
            </div>
            
        );
    }
}
