import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import {connect} from 'react-redux';
import * as Actions from '../actions/chatAction';
import Message from './Message';
import Rates from './controls/Rates';

@connect((store)=>store.analizator,(dispatch)=>bindActionCreators(Actions,dispatch))
class Analizator extends Component {
    
    componentWillMount() {
        this.props.startAnalizatorProcess();
    }
    
    render() {
        
        return (
            <div className="analizator">
             <div className="progress-bar" >
               <div className="progress-bar__wrap">
                <p>Checking storm and fire sensitivity</p>
                
                <div className="progress-bar__spinner">
                    {this.props.progress < 100 &&
                        <img src="./img/Spinner.svg" alt="image"/>
                    }
                </div>
                
               </div>
               <div className="progress" style={{width:this.props.progress+'%'}}/> 
              </div>
              {this.props.points.map((point,i)=><div key={i} className="point-item">
              <div className="point-item-name">{point.title}</div>
              <div className="point-item-rate">{/*point.rate  /*count of stars*/} 
                <Rates value={point.rate}/>
              </div>
              </div>)}
                
            </div>
        );
    }
}

export default Analizator;
