import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import {connect} from 'react-redux';
import * as Actions from '../actions/chatAction';


@connect((store)=>store.interlocutor||{},
(dispatch)=>bindActionCreators(Actions,dispatch))
class User extends Component {

    constructor(props){
        super(props);
    }
    
    
    componentWillMount() {
        this.props.getInterlocutorInfo();
    }

    componentWillReceiveProps(nextProps) {
        console.log(nextProps);
    }
    
    

    render() {
        
         var stars = [];
         if(this.props.info!=undefined)
          for(var i = 0; i<this.props.info.stars;i++)
          stars.push(<span key={i} className="star__item"></span>);

        return this.props.info!=undefined ?(
            
            <section className="user">
            <div className="user__photo">
                <img src={this.props.info.photo} alt="photo"/>
            </div>
            <div className="user__name">
                {this.props.info.username||'...'}
            </div>
            <div className="user__rate">
                <div className="star">
                    {stars}
                </div>
            </div>
            <div className="user__status">
            {this.props.info.status||'...'}
            </div>
        </section>
        ):null;
    }
}

export default User;
