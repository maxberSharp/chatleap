import CircularJSON from 'circular-json';
import React from 'react';
import { Router, Route, IndexRoute } from 'react-router';
import { browserHistory } from 'react-router';
import { Provider } from 'react-redux';
import thunkMiddleware from 'redux-thunk';
import apiMiddleware from './../middlewares/callApiMiddleware';
import { createStore,  compose,  applyMiddleware } from 'redux';
import reducers from '../reducers/index';
import Main from './Main';

export default class Application extends React.Component {

    render(){
        var initialState = {};
        if (window.localStorage.getItem('state')) {
            initialState = CircularJSON.parse(window.localStorage.getItem('state'))
        }

        let store = compose(applyMiddleware(apiMiddleware, thunkMiddleware),f => f)(createStore)(reducers, initialState);

        return <Provider store={store}>
            <Router history={browserHistory}>
                <Route path="/" component={Main} >                   
                    
                </Route>
            </Router>
        </Provider>;
    }

}