import React, { Component } from 'react';

class Header extends Component {
    render() {
        return (
            <header className="header">
            <div className="header__container">
                <div className="header__logo">
                    Chat Leap Test
                </div>
                <div className="header__button">
                    <a href="#" className="btn-reload">
                        Reload
                    </a>
                </div>
            </div>
        </header>
        );
    }
}

export default Header;
