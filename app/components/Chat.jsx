import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import {connect} from 'react-redux';
import * as Actions from '../actions/chatAction';
import Message from './Message';

@connect((store)=>store,
(dispatch)=>bindActionCreators(Actions,dispatch))

class Chat extends Component {
    
    constructor(props){
        super(props);
        this.state = {
            messages:[]
        }
    }

    
    componentWillReceiveProps(nextProps) {
        var messages = []
        var message = {};
        nextProps.messages.forEach((item) => {
            if(item.user.id == message.userId ){
                message.messages.push(item);
            }else{
                if(message.userId != undefined)
                   messages.push(message);
                message = item;
                message.userId = item.user.id;
                message.messages = [item];
            }
        });
        messages.push(message);
        this.setState({messages:messages});
        if(this.state.messages.length!= messages){
         setTimeout(()=>{ window.scrollTop = window.scrollHeight;
          document.getElementById("app").scrollTop = document.body.scrollHeight;
          console.log(document.body.scrollHeight,document.body.scrollTop);
         },500);
        }

    }
    
    

    render() {
        console.log(this.props);
        return (
            <section className="chat">
            {this.state.messages.map((msg,i) => msg.user != undefined ? <Message key={i} isowner={this.props.user.id == msg.user.id} msg={msg}/>:null)}
           
         
            {this.props.interlocutor.istyping &&<div className="chat__item chat__item_left">
            <div className="chat__user">
                <div className="chat__photo">
                    <img src={this.props.interlocutor.info.photo} alt="photo"/>
                </div>
            </div>
            <div className="chat__messages">
                <div className="bubble bubble_user bubble_preload">
                    <div className="preload">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
        </div>}
        </section>
        );
    }
}

export default Chat;
