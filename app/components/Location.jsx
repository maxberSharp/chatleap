import React, { Component,PropTypes } from 'react';
import Map from './controls/Map';
import GoogleMapReact from 'google-map-react';

const AnyReactComponent = ({ text }) => (
  <div style={{
    position: 'relative', color: '#e91e63', background: 'white',
    borderColor: '#e91e63', borderWidth: 3, borderStyle: 'solid', lineHeight: 3.5,
    textAlign: 'center',
    height: 40, width: 40, top: -20, left: -30, borderRadius: 40   
  }}>{text}</div>
)


class Location extends Component {

    constructor(props){
        super(props);
        this.state = {
            location:null
        }
    }

    componentWillMount() {
      try{
            var geocoder = new google.maps.Geocoder;
        var infowindow = new google.maps.InfoWindow;

        geocoder.geocode({'placeId': this.props.msg.text}, (results, status) => {
            if (status === 'OK') {
              if (results[0]) {
                  console.log(results[0]);
                  this.setState({ location: results[0].geometry.location, address: results[0].formatted_address });
          
              } else {
                console.log('No results found');
              }
            } else {
                console.log('Geocoder failed due to: ' + status);
            }
          });
        }catch(ex){
            console.log(ex);
        }
    }
    
   

    render() {
        return (

            <div className="chat__item chat__item_right">
                <div className="chat__user">
                    <div className="chat__photo">
                     <img src={this.props.msg.user.photo} alt="photo"/>
                    </div>
                </div>

                <div className="chat__messages">
                    <div className="bubble bubble__map">
                   { this.state.location && <Map lat={this.state.location.lat()} lng={this.state.location.lng()}/>}

                        <div className="bubble__map-text">
                           {this.state.address||''}
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}
Location.propTypes = {
    msg: PropTypes.object.isRequired
}
export default Location;
