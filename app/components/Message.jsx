import React, { Component,PropTypes } from 'react';
import Location from './Location';
import Analizator from './Analizator';
class Message extends Component {

    constructor(props){
        super(props);
       
    }

    render() {
        console.log(this.props.msg);
        return this.props.msg.type == 5 ? <Location msg={this.props.msg}/>:
               this.props.msg.type == 6 ? <Analizator />: (
                <div className={ this.props.isowner?'chat__item chat__item_right':'chat__item chat__item_left'}>
                <div className="chat__user">
                    <div className="chat__photo">
                        <img src={this.props.msg.user.photo} alt="photo"/>
                    </div>
                </div>
                <div className="chat__messages">
                    {this.props.msg.messages.map((msg,i)=>
                        <div key={i} className="bubble bubble_user">
                            <div className="bubble__text">
                                {msg.text}
                            </div>
                        </div>
                    )}                
                </div>
            </div>
        );
    }
}

Message.propTypes = {
    msg: PropTypes.object.isRequired,
    isowner: PropTypes.bool.isRequired
}

export default Message;
