import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';

const AnyReactComponent = ({ text }) => (
  <div style={{
    position: 'relative', color: '#e91e63', background: 'white',
    borderColor: '#e91e63', borderWidth: 3, borderStyle: 'solid', lineHeight: 3.5,
    textAlign: 'center',
    height: 40, width: 40, top: -20, left: -30, borderRadius: 40   
  }}>{text}</div>
)

class SimpleMap extends Component {


  render() {
    return (
      <GoogleMapReact
        defaultCenter={{lat: this.props.lat, lng: this.props.lng}}
        defaultZoom={14}
      >

        <AnyReactComponent
          lat={this.props.lat}
          lng={this.props.lng}
          text={'Ballon'}
        />
      </GoogleMapReact>
    );
  }
}


export default SimpleMap;