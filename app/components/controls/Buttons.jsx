import React, { Component, PropTypes } from 'react';

class Buttons extends Component {
      ;
    render() {
        return (
            <section className="controls">
                <div className="controls__container controls__container_np">
                    <div className="buttons-block">
                        <div className="buttons-block__wrap">
                            {Object.keys(this.props.options).map((key,i)=><a key={i} className={(i+1) < Object.keys(this.props.options).length ?'btn btn_br':'btn'} onClick={()=>this.props.onSendMessage(this.props.options[key])} href="javascript:void(0)">{key}</a>)}                
                        </div>
                    </div>
                </div>
        </section>
        );
    }
}
Buttons.propTypes = {
    options: PropTypes.object.isRequired,
    onSendMessage: PropTypes.func.isRequired
}

export default Buttons;
