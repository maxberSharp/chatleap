import React, { Component,PropTypes } from 'react';

class CheckBoxer extends Component {

    constructor(props){
        super(props);
        this.state = {
            values:{}
        }
    }

    handleChange(e){
        if (e.key == 'Enter'){
          this.props.onSendMessage(this.state.values);
          e.target.value = '';
        }
        
    }

    
    componentWillMount() {
        this.props.options.map((item,i)=>this.state.values[item] = false);
        this.setState({values:this.state.values});
    }
    

    onChange(is_checked,name){
        this.state.values[name] = is_checked;
        this.setState({values:this.state.values});
    }

    onSubmit() {
        let msg = ''
        for(var key in this.state.values) {
            if(this.state.values.hasOwnProperty(key)) {
                if (this.state.values[key]) {
                    msg = (msg)?msg +', ' + key: msg + key
                }
            }
        }
        this.props.onSendMessage(msg)
    }
    

    render() {
        return (
            <section className="controls controls_checkbox">
            <div className="controls__container">
            {this.props.options.map((item,i)=> <div key={i} className="checkbox">
                <label>
                    <input onChange={(e)=>this.onChange(e.target.checked,item)}   type="checkbox" />
                    <span></span>
                    {item}
                </label>
            </div>)}
            </div>
           {/* <div className="checkbox">
                <label>
                    <input onChange={(e)=>this.onChange(e.target.checked,'name1')}  type="checkbox" />
                    <span></span>
                    Fire alarm
                </label>
            </div>

            <div className="checkbox">
                <label>
                    <input onChange={(e)=>this.onChange(e.target.checked,'name2')}  type="checkbox" />
                    <span></span>
                    Burglar Alarm
                </label>
        </div>*/}
            <div className="controls__container controls__container_np">
                <button 
                    className="checkbox__button" 
                    onClick={() => this.onSubmit()}>ok</button>
            </div>
            
        </section>
        );
    }
}

CheckBoxer.propTypes = {   
    options: PropTypes.array.isRequired,
    onSendMessage: PropTypes.func.isRequired
}

export default CheckBoxer;
