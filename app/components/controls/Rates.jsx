import React, {Component} from 'react';


const stars = [1, 2, 3, 4, 5];
let x = 2.55
class Rates extends Component {
    constructor(props) {
        super(props);
    }

    classStar(item, x) {

        switch(true) {
            
            case item < x && item < Math.round(x):
                return 'full'; 
                break;
            
            case item == Math.round(x) :
                return 'half'; 
                break;
            default :
                return 'empty';
        }
    }

    render() {


        return(
            
            <div className="rates">
                
                {stars.map((item, index) => (
                    <span key={index} className={ this.classStar(item, this.props.value)}></span>                  
                ))}
            </div>   
        )
    }
}

export default Rates;