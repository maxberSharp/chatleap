import React, { Component,PropTypes } from 'react';

class TextArea extends Component {

    constructor(props){
        super(props);
        this.handleChange.bind(this);
    }

    handleChange(e){
        if (e.key == 'Enter'){
          this.props.onSendMessage(e.target.value);
          e.target.value = '';
        }
        
    }

    render() {
        return (

            <section className="controls">
                <div className="controls__container">
                    <textarea onKeyPress={(e)=>this.handleChange(e)} className="input-field" placeholder="Input Field"></textarea>
                </div>
            </section>

        );
    }
}

TextArea.propTypes = {
    onSendMessage: PropTypes.func.isRequired
}
export default TextArea;
