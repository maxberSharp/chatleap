import React, { Component,PropTypes } from 'react';

class Autocomplete extends Component {

    constructor(props){
        super(props);
        this.handleChange.bind(this);
        this.state = {
            results:[]
        }
    }

    handleChange(e){
       
        console.log(e.target.value);
        if(e.target.value.length>0)
        this.service.getQueryPredictions({ input: e.target.value }, (predictions, status)=>{
            console.log(predictions, status);
            if (status == google.maps.places.PlacesServiceStatus.OK) {
                this.setState({results:predictions});
              }
        });
    }

    
    componentDidMount() {
        this.service = new google.maps.places.AutocompleteService();

    }

    
    

    render() {
        console.log(this.state.results);
        return (

            <section className="controls">
                <div className="controls__container">
                    <div className="search_result">
                    {
                        this.state.results.map((item,i)=><div key={i} className="founded_item" onClick={()=>this.props.onSendMessage(item.place_id)}>{item.description}</div>)
                    }</div>

                     <textarea onKeyPress={(e)=>this.handleChange(e)} className="input-field" placeholder="Enter address"></textarea>

                </div>
            </section>

        );
    }
}

Autocomplete.propTypes = {
    onSendMessage: PropTypes.func.isRequired
}
export default Autocomplete;
