import React, { Component, PropTypes } from 'react';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import DatePicker from 'material-ui/DatePicker';

const muiTheme = getMuiTheme({
    datePicker : {
        headerColor: "#e91e63",
        selectColor: "#e91e63",
        color:       "#e91e63"
    },
    flatButton : {
        primaryTextColor: "#e91e63",
    }
})

class Datepicker extends Component {

    datePicker(i, date) {
        this.props.onSendMessage(date.toString());
        
    }

    render() {
        return (
        <MuiThemeProvider muiTheme={muiTheme}>
            <section className="controls">
                <div className="controls__container">
                    <DatePicker hintText="Enter date" ref={i => this.item = i} onChange={this.datePicker.bind(this)} className="datepicker" container="dialog" />
                </div>
            </section>
        </MuiThemeProvider>
        );
    }
}
Datepicker.propTypes = {
    onSendMessage: PropTypes.func.isRequired
}
export default Datepicker;
