import Application from './components/Application';
import ReactDom from 'react-dom';
import React from 'react';

document.addEventListener('DOMContentLoaded', function(){
    
    ReactDom.render(<Application />, document.getElementById('app'));

});